const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const schema = require("./schema/schema");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();

// allow cross-origin request
app.use(cors());

// connect to mlab database
mongoose.connect(
  "mongodb+srv://user1:test1@clustergraphql.zfxzx.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    useCreateIndex: true,
  }
);
mongoose.connection.once("open", () => {
  console.log("connected to database");
});

// if graphql called, app will call graphqlHTTP for help to resolve it.
app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql: true,
  })
);

app.listen(4000, () => {
  console.log("now listening to request 4000");
});
