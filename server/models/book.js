const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bookSchema = new Schema({
    name: String,
    genre: String,
    authorId: String,
});

module.exports = mongoose.model('Book', bookSchema);
// we making a collection (model) which is book, and inside of it looks like this Schema