import React, { useState } from "react";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import {
  getAuthorsQuery,
  addBookMutation,
  getBooksQuery,
} from "../queries/queries";

const AddBook = (props) => {
  const [authorState, setAuthorState] = useState({
    name: "",
    genre: "",
    authorId: "",
  });

  const displayAuthors = () => {
    const data = props.getAuthorsQuery;
    if (data.loading) {
      return <option>Loading Authors...</option>;
    } else {
      return data.authors.map((author) => {
        return (
          <option key={author.id} value={author.id}>
            {author.name}
          </option>
        );
      });
    }
  };

  const SubmitForm = (e) => {
    e.preventDefault();
    console.log("authorState", authorState);
    props.addBookMutation({
      variables: {
        name: authorState.name,
        genre: authorState.genre,
        authorId: authorState.authorId,
      },
      refetchQueries: [{ query: getBooksQuery }],
    });
  };

  return (
    <form id="add-book" onSubmit={SubmitForm}>
      <div className="field">
        <label>Book Name:</label>
        <input
          type="text"
          onChange={(e) => {
            setAuthorState({ ...authorState, name: e.target.value });
          }}
        />
      </div>

      <div className="field">
        <label>Genre:</label>
        <input
          type="text"
          onChange={(e) => {
            setAuthorState({ ...authorState, genre: e.target.value });
          }}
        />
      </div>

      <div className="field">
        <label>Author:</label>
        <select
          onChange={(e) => {
            setAuthorState({ ...authorState, authorId: e.target.value });
          }}
        >
          <option>Select author:</option>
          {displayAuthors()}
        </select>
      </div>
      <button>+</button>
    </form>
  );
};

export default compose(
  graphql(getAuthorsQuery, { name: "getAuthorsQuery" }),
  graphql(addBookMutation, { name: "addBookMutation" })
)(AddBook);
