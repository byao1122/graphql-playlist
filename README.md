# README #

### What is this repository for? ###

This is the tutorial about GraphQL Apollo server and client setup.

Tutorial URL : https://www.youtube.com/playlist?list=PL4cUxeGkcC9iK6Qhn-QLcXCXPQUov1U7f


### How do I get set up? ###

Part A ( Server )

x cd ./server

x nodemon app

Part B ( Client )

x cd ./client

x npm run start

### FAQ ###

1. How to debug if database unable to connect?

x Check server app.js file, there is mongoose config setup.

x Make sure the connection link is valid with correct username and password. 

x (Username: user1 / Password: test1)

x m1Lab Link : https://cloud.mongodb.com/v2/608d8572aa94426b4161f149#clusters

